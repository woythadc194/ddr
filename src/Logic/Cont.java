package Logic;

import java.awt.Container;
import java.util.ArrayList;

import GUI.Key;
import GUI.Screen;

public class Cont {

	private static Screen scr;
	private static ArrayList<Key> moving;
	public static final int SIZE = 50;
	
	
	public static final int[] CatchX = new int[] { SIZE*0, SIZE*1, SIZE*2, SIZE*3 };
	public static final int[] CatchY = new int[] { SIZE,   SIZE,   SIZE,   SIZE };
	
	public static final int[] ThrowX = new int[] { SIZE*0, SIZE*1, SIZE*2, SIZE*3 };
	public static final int[] ThrowY = new int[] { SIZE*9, SIZE*9, SIZE*9, SIZE*9 };
	
	private static Key[] catchers = new Key[4];
	
	public static Screen getScreen(){
		return Cont.scr;
	}
	public static void setScreen( Screen scr){
		Cont.scr = scr;
	}
	public static void addMoving(Key k){
		moving.add(k);
	}
	public static ArrayList<Key> getMoving(){
		return Cont.moving;
	}
	
	public Cont(){
		moving = new ArrayList<Key>();
	}
	public static Key[] getCatchers() {
		return catchers;
	}
}