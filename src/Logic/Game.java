package Logic;

import GUI.Key;
import GUI.Moving;
import GUI.Screen;

public class Game{
	
	private static Cont cont;
	private static Screen scr;
	private static long startPaint;
	private static long currentPaint;
	private static long startAdd;
	private static long currentAdd;
	private static boolean clearingPiece;
	
	public static void main (String [] args){
		cont = new Cont();
		scr = new Screen(cont);
		Cont.setScreen(scr);
		play();
	}
	
	
	public static void play(){
	
		new Moving(cont);
		startPaint = System.currentTimeMillis();
		startAdd = System.currentTimeMillis();
		clearingPiece = false;
		
		while(true){
			currentPaint = System.currentTimeMillis();
			currentAdd = System.currentTimeMillis();
			if( currentPaint-startPaint > 10 ){
				startPaint = System.currentTimeMillis();
				
				clearingPiece = true;
				for( Key k : Cont.getMoving() ){	
					k.setPos(k.getX(), k.getY()-(Cont.SIZE/16));
					if( k.getY() < 0-Cont.SIZE ){
						Cont.getMoving().remove(k);
						Cont.getScreen().remove(k);
					}
				}
				clearingPiece = false;
				scr.repaint();
			}

			if( currentAdd-startAdd > 500 ){
				startAdd = System.currentTimeMillis();
				new Moving(cont);
			}		
		}
	}
	
	public static void checkKeyPress(int keyCode) {
		if( keyCode == 37 ){
			Cont.getCatchers()[ 0 ].keyOn( true );
		}else if( keyCode == 38 ){
			Cont.getCatchers()[ 2 ].keyOn( true );
		}else if( keyCode == 39 ){
			Cont.getCatchers()[ 1 ].keyOn( true );
		}else if( keyCode == 40 ){
			Cont.getCatchers()[ 3 ].keyOn( true );
		}
	}
	public static void checkKeyRelease(int keyCode) {
		if( 37 <= keyCode && keyCode <= 40){
			while(clearingPiece){
				;
			}
			//Left
			if( keyCode == 37 )
				Cont.getCatchers()[ 0 ].keyOn( false );
			//Right
			else if( keyCode == 39 )
				Cont.getCatchers()[ 1 ].keyOn( false );
			//Up
			else if( keyCode == 38 )
				Cont.getCatchers()[ 2 ].keyOn( false );
			//Down
			else if( keyCode == 40 )
				Cont.getCatchers()[ 3 ].keyOn( false );
			checkZone( keyCode );
		}
	}
	public static void checkZone( int keyCode ){
		Key target = null;
		int height = Cont.SIZE*10;
		for(Key k : Cont.getMoving() ){
			if( k.getY() < height ){
				target = k;
				height = k.getY();
			}
		}
		
		if( Cont.SIZE * 0.85 < height && height < Cont.SIZE * 1.15  ){
			System.out.println( "Perfect" );
			Cont.getScreen().remove( target );
			Cont.getMoving().remove( target );
		}else if( Cont.SIZE * 0.65 < height && height < Cont.SIZE * 1.35 ){
			System.out.println( "Great" );
			Cont.getScreen().remove( target );
			Cont.getMoving().remove( target );
		}else if( Cont.SIZE * 0.5 < height && height < Cont.SIZE * 1.5  ){
			System.out.println( "Okay" );
			Cont.getScreen().remove( target );
			Cont.getMoving().remove( target );
		}
	}
}