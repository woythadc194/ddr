package GUI;

import java.util.Random;

import Logic.Cont;

@SuppressWarnings("serial")
public class Moving extends Key{
	
	private int index;
	
	public Moving(Cont cont){
		super(cont);
		
		Random rand = new Random();
		this.index = rand.nextInt(4); 
		this.setDirection( index+37 );
		setPos(Cont.ThrowX[index], Cont.ThrowY[index]);
		Cont.getScreen().add(this);
		Cont.addMoving( this );
		this.setCatcher( false );
	}
}