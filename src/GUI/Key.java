package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JButton;

import Logic.Cont;

@SuppressWarnings("serial")
public class Key extends JButton{


	//private static Cont cont;
	protected int direction;
	private boolean catcher = false;
	private boolean keyOn = false;
	
	public Key(Cont cont){
		super();
		//Key.cont = cont;
		this.setSize( new Dimension(Cont.SIZE, Cont.SIZE) );		
	}
	public void setDirection(int dir){
		this.direction = dir;
	}
	public void setPos( int x, int y ){
		this.setLocation( x, y );
	}
	public void setVis( boolean b ){
		this.setVisible( b );
	}
	public void setCatcher( boolean b ){
		this.catcher = b;
	}
	public void keyOn( boolean b ){
		this.keyOn = b;
	}
	public void paint( Graphics g ) {
		g.setColor( Color.black );
		g.fillRect( 0, 0, Cont.SIZE, Cont.SIZE );
		if( catcher ){
			if( keyOn )
				g.setColor( Color.red );
			else
				g.setColor( Color.blue );
		} else {
			g.setColor( Color.white );
		}
		g.fillRect( 2, 2, Cont.SIZE-4, Cont.SIZE-4 );
		
		int z = Cont.SIZE;
		g.setColor( Color.black );
		
		//Left
		if( direction == 37 )
			g.fillPolygon( new int[] {z*1/5, z*1/2, z*1/2, z*4/5, z*4/5, z*1/2, z*1/2, z*1/5}, new int[] {z*1/2, z*4/5, z*3/5, z*3/5, z*2/5, z*2/5, z*1/5, z*1/2}, 8);
		//Right
		if( direction == 38 )
			g.fillPolygon( new int[] {z*4/5, z*1/2, z*1/2, z*1/5, z*1/5, z*1/2, z*1/2, z*4/5}, new int[] {z*1/2, z*1/5, z*2/5, z*2/5, z*3/5, z*3/5, z*4/5, z*1/2}, 8);
		//Up
		if( direction == 39 )
			g.fillPolygon( new int[] {z*1/2, z*1/5, z*2/5, z*2/5, z*3/5, z*3/5, z*4/5, z*1/2}, new int[] {z*1/5, z*1/2, z*1/2, z*4/5, z*4/5, z*1/2, z*1/2, z*1/5}, 8);
		//Down
		if( direction == 40 )
			g.fillPolygon( new int[] {z*1/2, z*4/5, z*3/5, z*3/5, z*2/5, z*2/5, z*1/5, z*1/2}, new int[] {z*4/5, z*1/2, z*1/2, z*1/5, z*1/5, z*1/2, z*1/2, z*4/5}, 8);
			
	}
}
