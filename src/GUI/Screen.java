package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import Logic.Cont;
import Logic.Game;

@SuppressWarnings("serial")
public class Screen extends JPanel implements KeyListener{

	//private static Cont cont;
	private JFrame frame;
	
	
	public Screen(Cont cont){
		super();
		//Screen.cont = cont;
		this.setPreferredSize( new Dimension( Cont.SIZE*4, Cont.SIZE*3*3 ) );
		this.setBackground( Color.black );
		this.setLayout( null );
		
		
		for(int i=0; i<4; i++){
			Key k = new Key( cont );
			k.setPos( Cont.CatchX[ i ], Cont.CatchY[ i ] );
			k.setDirection( i + 37 );
			k.setCatcher( true );
			this.add( k );
			Cont.getCatchers()[ i ] = k;
		}
		
		
		
		frame = new JFrame();
		frame.setVisible( true );
		frame.setResizable( false );
		frame.setLocation( 500, 100 );
		frame.add( this );
		frame.addKeyListener( this );
		frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		frame.addWindowListener( new WindowAdapter(){
			public void windowClosing( WindowEvent w ){
				System.exit( 0 );
			}
		} );
		frame.pack();
	}


	@Override
	public void keyPressed(KeyEvent e) {
		Game.checkKeyPress( e.getKeyCode() );		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		Game.checkKeyRelease( e.getKeyCode() );
	}
	@Override
	public void keyTyped(KeyEvent e) { /* TODO Auto-generated method stub */ }
	
}
